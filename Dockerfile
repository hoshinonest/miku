
FROM node:16
RUN apt-get update
RUN apt-get install sox libsox-fmt-all -y

# Create the directory!
RUN mkdir -p /usr/src/bot
WORKDIR /usr/src/bot

# Copy and Install our bot
COPY package.json /usr/src/bot
COPY package-lock.json /usr/src/bot
RUN npm install -g npm@9.4.0
RUN npm install
RUN npm install -g ts-node@10.9.1

# Our precious bot
COPY . /usr/src/bot
CMD ["ts-node", "./src/discord.ts"]
