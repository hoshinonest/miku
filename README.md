## Miku project

#### Setup

1. Copy `.env.example` to a new `.env` file and add your API keys:

```
OPENAI_API_KEY=sk-your-key
REPLICATE_KEY=your-replicate-key
NGROK_KEY=your-ngrook-key
ELEVEN_KEY=your-eleven-labs-api-key
ELEVEN_VOICE_KEY=eleven-labs-voice-key
USE_ENGLISH=1
DISCORD_TOKEN=discord-bot-token
```
2. Install system dependencies

Ubuntu:
```
apt install sox libsox-fmt-all
```

Mac:
```
brew install sox
```

3. Run `npm install`
4. Run `npm start` to start interacting with Miku

#### Preview
![Preview Video](preview.webm)


#### Discord bot
To run the discord bot
```
npm run discord
```