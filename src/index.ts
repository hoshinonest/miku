require("dotenv").config();
import ChatManager from "./libs/ChatManger";
import { terminal as term } from "terminal-kit";
import KeyListener from "./libs/KeyListener";
import AudioRecorder from "./libs/AudioRecorder";
import { SpeechToText } from "./libs/ReplicateAPI";
import AudioSynthesizer from "./libs/AudioSynthesizer";

async function start() {
  // chat manager
  const chatManger = new ChatManager({
    openAIKey: process.env.OPENAI_API_KEY || "",
    dataSrc: "./src/data",
    subject: "Fuutarou",
    botSubject: "Miku",
  });
  await chatManger.initConversation();

  // whisper
  const speechToText = new SpeechToText();

  term("Start chatting with ").green("Miku\n");

  await term.drawImage(__dirname + "/assets/image.png", {
    shrink: { width: 30, height: 30 },
  });

  term.blue('Press "e" to start recording, end with "w"...\n');

  const audioRecorder = new AudioRecorder();

  term.magenta("\nYou: ");
  new KeyListener({
    startKey: "e",
    endKey: "w",
    onStart: () => {
      audioRecorder.start();
    },
    onEnd: async () => {
      const filename = audioRecorder.end();
      const spinner = await term.spinner();
      let result = "O-oh, could you repeat?";
      try {
        const { text } = await speechToText.translate(filename);
        await spinner.animate(0);
        term.backDelete();
        term.white(text);

        const spinner2 = await term.spinner();
        result = await chatManger.sendMessage(text);
        await spinner2.animate(0);
      } catch (e) {
        term.red("An error occurred!\n");
        console.log(e);
      }

      const audioSynthesizer = new AudioSynthesizer(result);
      await audioSynthesizer.synthesizeAndPlay();

      
      term.backDelete();
      term.green("\nMiku: ").gray(result).eraseLineAfter();
      term.magenta("\nYou: ");
    },
  });

  // function readFromUser() {
  //   term.magenta("\nYou:");
  //   term.inputField(async (error, _input) => {
  //     if(_input === ':q') exit();
  //     const input = (_input || '').replace('\n', '');
  //     term('\n');
  //     const spinner = await term.spinner();
  //     try {
  //       const result = await chatManger.sendMessage(input);
  //       await spinner.animate(0);
  //       term.backDelete();
  //       term.green("Miku: ").gray(result).eraseLineAfter();
  //     } catch(e) {
  //       term.red('An error occurred!\n');
  //     }
  //     readFromUser();
  // });
  // }

  // readFromUser();
}
start();
