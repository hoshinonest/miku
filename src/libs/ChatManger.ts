import OpenAIManager from "./OpenAIManger";
import PromptManager from "./PromptManager";

interface ChatMangerConfig {
  openAIKey: string
  dataSrc: string
  subject: string
  botSubject: string
}

export default class ChatManager {
  private config: ChatMangerConfig;
  private promptManager: PromptManager;
  private openaiManger: OpenAIManager;

  constructor(config: ChatMangerConfig) {
    this.config = config;
    this.promptManager = new PromptManager(this.config.dataSrc);
    this.openaiManger = new OpenAIManager({
      apiKey: this.config.openAIKey,
      stop: [
        `${this.config.subject}: `,
        `${this.config.botSubject}: `,
      ]
    })
  }

  initConversation(): Promise<void> {
    return this.promptManager.loadBasePrompt();
  }

  async sendMessage(message: string): Promise<string> {
    this.promptManager.addHistory(`\n${this.config.subject}: ${message}\n${this.config.botSubject}:`);
    const prompt = this.promptManager.getCurrentPrompt();
    const botResponseRaw = await this.openaiManger.createCompletion(prompt);
    this.promptManager.addHistory(botResponseRaw);
    return botResponseRaw.replace(`\n${this.config.botSubject}: `, '');
  }
}