import FileManager from "./FileManager";

export default class PromptManager {
  private dataSrc: string; 
  private basePrompt: string = '';
  private localHistory: string = '';

  constructor(dataSrc: string) {
    this.dataSrc = dataSrc;
  }

  async loadBasePrompt(): Promise<void> {
    const useEnglish = process.env.USE_ENGLISH === '1' ? "_en" : "";
    this.basePrompt = await FileManager.readFile(`${this.dataSrc}/prompt${useEnglish}.txt`);
    const history = await FileManager.readAllFilesWithPrefix('history_', this.dataSrc);
    this.basePrompt += history.reduce(
      (prev: string, current: string) => prev + current, this.basePrompt
    );
    this.localHistory = '';
  }

  addHistory(history: string) {
    this.localHistory += history;
  }

  getCurrentPrompt(): string {
    return this.basePrompt + this.localHistory;
  }
}