import { readFile } from "fs/promises";

export default abstract class FileManager {

  static async readFile(file: string): Promise<string> {
    return readFile(file)
      .then(file => file.toString().replace('\n', '\\n'))
      .catch(() => '');
  }

  static async readAllFilesWithPrefix(prefix: string, folder: string): Promise<string[]> {
    return [];
  }
}