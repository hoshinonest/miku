import { Configuration, OpenAIApi } from "openai";


async function start() {
  
}

start();

interface OpenAIManagerConfig {
  apiKey: string;
  stop: string[]
}

export default class OpenAIManager {
  private config: OpenAIManagerConfig;
  private openai: OpenAIApi;

  constructor(config: OpenAIManagerConfig) {
    this.config = config;
    const configuration = new Configuration({
      apiKey: this.config.apiKey,
    });
    this.openai = new OpenAIApi(configuration);
  }

  async createCompletion(prompt: string): Promise<string> {
    const completion = await this.openai.createCompletion({
      model: "text-davinci-003",
      prompt,
      temperature: 0.9,
      max_tokens: 150,
      top_p: 1,
      frequency_penalty: 0,
      presence_penalty: 0.6,
      stop: this.config.stop,
      
    });
    const choices = completion?.data?.choices || [];

    return choices.length ? (choices[0].text || '') : '';
  }
}