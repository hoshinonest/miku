import axios, { AxiosResponse } from 'axios';
import fs from "fs";
require("dotenv").config();

interface GradioAPIParams {
  name: string
  url: string
  authKey?: string
}

export class GradioAPI<InputType, OutputType> {
  public name: string;
  public url: string;
  private authKey: string

  constructor({ url, name, authKey }: GradioAPIParams) {
    this.name = name;
    this.url = url;
    this.authKey = authKey || '';
  }

  protected async predict(input: InputType): Promise<AxiosResponse<OutputType>> {
    return axios<OutputType>({
      url: this.url,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authKey}`
      },
      data: input
    })
  }
}

interface SpeechToTextInput {
  data: {
    name: string;
    data: string;  
  }[]
}

interface SpeechToTextResult {
  data: string[]
  duration: number
}

export class WhisperSpeech2Text extends GradioAPI<SpeechToTextInput, SpeechToTextResult> {
  constructor(url: string, authKey: string) {
    super({
      name: 'whisper',
      url,
      authKey,
    });
  }
  async translate(filename: string): Promise<string> {
    const prediction = await this.predict({
      data: [
        {
          name: filename,
          data: 'data:audio/ogg;base64,' + fs.readFileSync(filename,  {encoding: 'base64'}),
        }
      ]
    });

    return prediction.data.data[0] || '';
  }
}