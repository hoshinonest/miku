import ngrok from 'ngrok';
import express from 'express';
import bodyParser from 'body-parser';

const data: {
  url: string, subscription?: (body: any) => void
} = {
  url: '',
  subscription: undefined
};

var server = express();
server.use(bodyParser.json());
server.post("/miku-webhook", (req, res) => {
  data.subscription && data.subscription(req.body);
  res.sendStatus(200);
  res.send();
});
server.listen(4242);

// NGOCKL
(async function() {
  data.url = await ngrok.connect({
    authtoken: process.env.NGROK_KEY,
    addr: 4242
  });
})();


export const getUrl = () => data.url + '/miku-webhook';

export const subscribe = (fn: (body: any) => void) => {
  data.subscription = fn;
};