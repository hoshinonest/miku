import axios, { AxiosResponse } from 'axios';
import fs from "fs";
import { subscribe, getUrl } from './NgrokServer';
require("dotenv").config();

const REPLICATE_URL = 'https://api.replicate.com/v1/predictions';
const REPLICATE_KEY = process.env.REPLICATE_KEY;

interface ReplicateAPIParams {
  model: string,
  version: string
}

export class ReplicateAPI<InputType> {
  private model: string;
  private version: string;

  constructor({model, version}: ReplicateAPIParams) {
    this.model = model;
    this.version = version;
  }

  protected async predict(input: InputType): Promise<AxiosResponse> {
    return axios<{type: 'object'}>({
      url: REPLICATE_URL,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Token ${REPLICATE_KEY}`
      },
      data: {
        version: this.version,
        input,
        webhook_completed: getUrl()
      }
    })
  }
}

interface SpeechToTextInput {
  audio: string,
  'model': string,
  'transcription': string,
  'translate': boolean,
  'temperature': number,
  'condition_on_previous_text': boolean,
  'temperature_increment_on_fallback': number,
  'compression_ratio_threshold': number,
  'logprob_threshold': number,
  'no_speech_threshold': number,
  'language': string,
}

interface SpeechToTextResult {
  text: string
  mood: string
  langauge: string
}

export class SpeechToText extends ReplicateAPI<SpeechToTextInput> {
  private data: {
    sub?: (body: any) => void
  } = {};
  constructor() {
    super({
      model: 'openai/whisper',
      version: '30414ee7c4fffc37e260fcab7842b5be470b9b840f2b608f5baa9bbef9a259ed'
    });
    subscribe((body: any) => {
      this.data.sub && this.data.sub(body);
    });
  }
  async translate(filename: string): Promise<SpeechToTextResult> {
    const prediction = await this.predict({
        audio: 'data:audio/ogg;base64,' + fs.readFileSync(filename,  {encoding: 'base64'}),
        'model': "base",
        'transcription': "plain text",
        'language': process.env.USE_ENGLISH === '1' ? 'en' : 'es',
        'translate': false,
        'temperature': 0,
        'condition_on_previous_text': true,
        'temperature_increment_on_fallback': 0.2,
        'compression_ratio_threshold': 2.4,
        'logprob_threshold': -1,
        'no_speech_threshold': 0.6,      
    });
    console.log()

    return new Promise((res, rej) => {
      this.data.sub = (body: {id: string, output: {transcription: string, detected_language: string}}) => {
        if (body.id === prediction.data?.id) {
          res({
            text: body?.output?.transcription || '',
            mood: 'neutral',
            langauge: body?.output?.detected_language || '',
          })  
        } else {
          res({
            text: '',
            mood: 'neutral',
            langauge: body?.output?.detected_language || '',
          })
        }
      };
    })
  }
}