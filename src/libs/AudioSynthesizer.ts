
import axios from 'axios';
import fs from 'fs';

var player = require('play-sound')({})
require("dotenv").config();

export default class AudioSynthesizer {
  text: string;

  constructor(text: string) {
    this.text = this._cleanText(text);
    text.replace(/ *\([^)]*\) */g, "");
  }

  _cleanText(text: string) {
    let cleanText = "";
    let lastOpen: undefined | string = undefined;
    for (var x = 0; x < text.length; x++)
    {
        var ch = text.charAt(x);

        if (lastOpen == '(' && ch == ')') {lastOpen = undefined; continue;}
        if (lastOpen == '[' && ch == ']') {lastOpen = undefined; continue;}
        if (lastOpen == '-' && ch == '-') {lastOpen = undefined; continue;}
        if (lastOpen == '*' && ch == '*') {lastOpen = undefined; continue;}

        if (ch == '(' || ch == '[' || ch == '-' || ch == "*") {
          lastOpen = ch;
          continue;
        }

        if (!lastOpen) {
          cleanText += ch;
        }

        
    }

    return cleanText;
  }

  async synthesizeAndPlay() {
    const voiceId = process.env.ELEVEN_VOICE_KEY;
    const requestUrl = `https://api.elevenlabs.io/v1/text-to-speech/${voiceId}`;
    await axios.post(requestUrl, { text: this.text }, {
      headers: {
        'Accept': 'audio/mpeg',
        'xi-api-key': process.env.ELEVEN_KEY,
      },
      'responseType': "arraybuffer"
    }).then((response) => {
      fs.writeFileSync('temp/ans.mp3', response.data);
        player.play('temp/ans.mp3', () => {
      })
    })
    .catch((err) => {
      console.log("Error: ", err);
    });
  }

  async synthesizeAndFilename(): Promise<string> {
    const voiceId = process.env.ELEVEN_VOICE_KEY;
    const requestUrl = `https://api.elevenlabs.io/v1/text-to-speech/${voiceId}`;
    return await axios.post(requestUrl, { text: this.text }, {
      headers: {
        'Accept': 'audio/mpeg',
        'xi-api-key': process.env.ELEVEN_KEY,
      },
      'responseType': "arraybuffer"
    }).then((response) => {
      fs.writeFileSync('temp/ans.mp3', response.data);
      return 'temp/ans.mp3';
    })
    .catch((err) => {
      console.log("Error: ", err);
      return '';
    });
  }
}