import { Client, GatewayIntentBits, User, ActionRowBuilder, ButtonBuilder, ButtonStyle, Events, Interaction } from 'discord.js';
import { joinVoiceChannel, createAudioPlayer, createAudioResource, EndBehaviorType, VoiceConnectionStatus, VoiceConnection, AudioPlayer } from '@discordjs/voice';
import { OpusEncoder } from '@discordjs/opus';
import { FileWriter } from 'wav';
import { SpeechToText } from "./libs/ReplicateAPI";
import AudioSynthesizer from "./libs/AudioSynthesizer";
import ChatManager from './libs/ChatManger';
import { Transform } from 'stream';
import { WhisperSpeech2Text } from './libs/GradioAPI';
import { randomUUID } from 'crypto';
var audioConverter = require("audio-converter");

require("dotenv").config();

class OpusDecodingStream extends Transform {
  encoder

  // @ts-ignore
  constructor(options, encoder) {
      super(options)
      this.encoder = encoder
  }
  // @ts-ignore
  _transform(data, encoding, callback) {
      this.push(this.encoder.decode(data))
      callback()
  }
}


const client = new Client({ intents: [
  GatewayIntentBits.Guilds,
  GatewayIntentBits.GuildMessages,
  GatewayIntentBits.MessageContent,
  GatewayIntentBits.GuildVoiceStates
] });

const speechToText = new WhisperSpeech2Text(
  process.env.WHISPER_AUDIO_API || '',
  process.env.WHISPER_AUDIO_API_KEY || ''
);
const chatManger = new ChatManager({
  openAIKey: process.env.OPENAI_API_KEY || "",
  dataSrc: "./src/data",
  subject: "Fuutarou",
  botSubject: "Miku",
});

client.on('ready', async () => {
  console.log(`Logged in as ${client.user?.tag}!`);
});

function getDisplayName(userId: string, user?: User) {
	return user ? `${user.username}_${user.discriminator}` : userId;
}

let connection: VoiceConnection | null;
const player = createAudioPlayer();

player.once('idle', () => {
  console.log('idle');
});

const destroyConnection = () => {
  console.log('connections', connection?.receiver?.subscriptions);
  connection?.disconnect();
  connection?.destroy();
  connection = null;
}

client.on('messageCreate', async message => {
  console.log('read: ', message.content);

  if (message.content === 'talk') {
    if (connection) {
      destroyConnection();
    }
    await chatManger.initConversation();
    const channel = message.member?.voice.channel;
    if(!channel || !message?.guild?.voiceAdapterCreator) return console.error("The channel does not exist!");
    connection = joinVoiceChannel({
      channelId: channel.id,
      guildId: message.guild?.id || '',
      adapterCreator: message.guild?.voiceAdapterCreator,
      selfDeaf: false,
    });
    const resource = createAudioResource(__dirname +'/assets/test.mp3');
    player.play(resource);
    connection.subscribe(player)

    const buttonId = 'record_' + randomUUID();
    const row = new ActionRowBuilder<ButtonBuilder>()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('primary')
          .setLabel('Record audio')
          .setCustomId(buttonId)
          .setStyle(ButtonStyle.Primary),
      );
    await message.reply({ content: '', components: [row] });

    const listenAudio = (memberId: string, memberUser: User) => {
      const start_timer = Date.now();
      const filename = `./recordings/${Date.now()}-${getDisplayName(memberId, memberUser)}.ogg`;
      const encoder = new OpusEncoder(16000, 1)
      const opusStream = connection?.receiver.subscribe(memberId, {
        end: {
          behavior: EndBehaviorType.AfterSilence,
          duration: 1000
        }
      })
      .pipe(new OpusDecodingStream({}, encoder))
      .pipe(new FileWriter(filename, {
          channels: 1,
          sampleRate: 16000
      }))


      opusStream?.once("end", async () => {
        const labmda_timer = Date.now() - start_timer;
        console.log('recording ended', labmda_timer);
        if(labmda_timer < 1000) return;
        player?.play(createAudioResource(__dirname +'/assets/waiting.mp3'));
        await audioConverter(filename, `${filename}.mp3`);
        console.log('0/3 file', filename)
        const recordingText = await speechToText.translate(filename);
        console.log('1/3 whisper', recordingText)
        const responseText = await chatManger.sendMessage(recordingText);
        console.log('2/3 chatgpt', responseText)
        const responseAudioFilename =  await new AudioSynthesizer(responseText).synthesizeAndFilename();
        console.log('3/3 elevenlabs')
        player.play(createAudioResource(responseAudioFilename));
      }); 
    }

    client.on(Events.InteractionCreate, (interaction: Interaction) => {
      if (!interaction.isButton()) return;
      if (message.member?.id !== interaction.user.id) return;
      console.log('call listen audio', interaction.customId, buttonId);
      if (interaction.customId !== buttonId) return;
      listenAudio(message.member.id, message.member.user);
    });
  }
  if (message.content === 'stop') {
    destroyConnection();
  }
});

client.login(process.env.DISCORD_TOKEN || '');